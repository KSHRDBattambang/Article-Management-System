package com.chivoin.spring.controllers.rest;

import com.chivoin.spring.entities.User;
import com.chivoin.spring.entities.filter.UserFilter;
import com.chivoin.spring.entities.form.UserForm;
import com.chivoin.spring.entities.response.*;
import com.chivoin.spring.entities.response.failure.*;
import com.chivoin.spring.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Created by Obi-Voin Kenobi on 05-Jul-17.
 */
@RestController
@RequestMapping("v1/api/user")
public class UserRestController {

    private HttpStatus httpStatus;
    private UserService userService;

    @Autowired
    public UserRestController(UserService userService) {
        this.userService = userService;
    }

    @GetMapping("/find-all-users")
    private ResponseEntity<Response<User>> findAllUsers() {
        Response<User> response;

        try {
            List<User> userList = userService.findAllUsers();
            if (!userList.isEmpty()) {
                httpStatus = HttpStatus.OK;
                response = new ResponseRecords<>(
                        HTTPMessage.getSuccessMessage(Table.USERS, Transaction.SUCCESS_RETRIEVED),
                        true,
                        userList
                );
            }
            else {
                httpStatus = HttpStatus.NOT_FOUND;
                response = new ResponseRecordsFailure<>(
                        HTTPMessage.getFailureMessage(Table.USERS, Transaction.FAILURE_RETRIEVE),
                        false,
                        ResponseHTTPStatus.RECORD_NOT_FOUND
                );
            }
        }
        catch (Exception e) {
            e.printStackTrace();
            httpStatus = HttpStatus.INTERNAL_SERVER_ERROR;
            response = new ResponseRecordsFailure<>(
                    HTTPMessage.getFailureMessage(Table.USERS, Transaction.FAILURE_RETRIEVE),
                    false,
                    ResponseHTTPStatus.INTERNAL_SERVER_ERROR
            );
        }

        return new ResponseEntity<>(response, httpStatus);
    }

    @PostMapping("/filter")
    private ResponseEntity<Response<User>> filterUsers(@RequestBody UserFilter user){
        Response<User> response;

        try {
            List<User> userList = userService.filterUsers(user);
            if (!userList.isEmpty()) {
                httpStatus = HttpStatus.OK;
                response = new ResponseRecords<>(
                        HTTPMessage.getSuccessMessage(Table.USERS, Transaction.SUCCESS_RETRIEVED),
                        true,
                        userList
                );
            }
            else {
                httpStatus = HttpStatus.NOT_FOUND;
                response = new ResponseRecordsFailure<>(
                        HTTPMessage.getFailureMessage(Table.USERS, Transaction.FAILURE_RETRIEVE),
                        false,
                        ResponseHTTPStatus.RECORD_NOT_FOUND
                );
            }
        }
        catch (Exception e) {
            e.printStackTrace();
            httpStatus = HttpStatus.INTERNAL_SERVER_ERROR;
            response = new ResponseRecordsFailure<>(
                    HTTPMessage.getFailureMessage(Table.USERS, Transaction.FAILURE_RETRIEVE),
                    false,
                    ResponseHTTPStatus.INTERNAL_SERVER_ERROR
            );
        }

        return new ResponseEntity<>(response, httpStatus);
    }

    @GetMapping("/{uuid}")
    private ResponseEntity<Response<User>> findUsersByUUID(@PathVariable("uuid") String uuid) {
        Response<User> response;

        try {
            User user = userService.findUsersByUUID(uuid);
            if (user != null) {
                httpStatus = HttpStatus.OK;
                response = new ResponseRecord<>(
                        HTTPMessage.getSuccessMessage(Table.USERS, Transaction.SUCCESS_RETRIEVED),
                        true,
                        user
                );
            }
            else {
                httpStatus = HttpStatus.NOT_FOUND;
                response = new ResponseRecordFailure<>(
                        HTTPMessage.getFailureMessage(Table.USERS, Transaction.FAILURE_RETRIEVE),
                        false,
                        ResponseHTTPStatus.RECORD_NOT_FOUND
                );
            }
        }
        catch (Exception e) {
            e.printStackTrace();
            httpStatus = HttpStatus.INTERNAL_SERVER_ERROR;
            response = new ResponseRecordFailure<>(
                    HTTPMessage.getFailureMessage(Table.USERS, Transaction.FAILURE_RETRIEVE),
                    false,
                    ResponseHTTPStatus.INTERNAL_SERVER_ERROR
            );
        }

        return new ResponseEntity<>(response, httpStatus);
    }

    @PutMapping("/{uuid}/status/{status}")
    private ResponseEntity<Response<User>> updateUserStatus(@PathVariable("uuid") String uuid, @PathVariable("status") String status){
        Response<User> response;

        try {
            if (userService.updateUserStatus(uuid, status)) {
                httpStatus = HttpStatus.OK;
                response = new Response<>(
                        HTTPMessage.getSuccessMessage(Table.USERS, Transaction.SUCCESS_UPDATED),
                        true
                );
            }
            else {
                httpStatus = HttpStatus.NOT_FOUND;
                response = new ResponseFailure<>(
                        HTTPMessage.getFailureMessage(Table.USERS, Transaction.FAILURE_UPDATE),
                        false,
                        ResponseHTTPStatus.FAIL_UPDATED
                );
            }
        }
        catch (Exception e) {
            e.printStackTrace();
            httpStatus = HttpStatus.INTERNAL_SERVER_ERROR;
            response = new ResponseRecordFailure<>(
                    HTTPMessage.getFailureMessage(Table.USERS, Transaction.FAILURE_UPDATE),
                    false,
                    ResponseHTTPStatus.INTERNAL_SERVER_ERROR
            );
        }

        return new ResponseEntity<>(response, httpStatus);
    }

    @PutMapping
    private ResponseEntity<Response<User>> updateUser(@RequestBody UserForm user) {
        Response<User> response;

        try {
            User updateUser = userService.updateUser(user);
            if (updateUser != null) {
                httpStatus = HttpStatus.OK;
                response = new ResponseRecord<>(
                        HTTPMessage.getSuccessMessage(Table.USERS, Transaction.SUCCESS_UPDATED),
                        true,
                        updateUser
                );
            }
            else {
                httpStatus = HttpStatus.BAD_REQUEST;
                response = new ResponseRecordFailure<>(
                        HTTPMessage.getFailureMessage(Table.USERS, Transaction.FAILURE_UPDATE),
                        false,
                        ResponseHTTPStatus.FAIL_UPDATED
                );
            }
        }
        catch (Exception e) {
            e.printStackTrace();
            httpStatus = HttpStatus.INTERNAL_SERVER_ERROR;
            response = new ResponseRecordFailure<>(
                    HTTPMessage.getFailureMessage(Table.USERS, Transaction.FAILURE_UPDATE),
                    false,
                    ResponseHTTPStatus.INTERNAL_SERVER_ERROR
            );
        }

        return new ResponseEntity<Response<User>>(response, httpStatus);
    }

    @DeleteMapping("/{uuid}/delete")
    private ResponseEntity<Response<User>> deleteUserByUUID(@PathVariable("uuid") String uuid) {
        Response<User> response;

        try {
            if (userService.deleteUserByUUID(uuid)) {
                httpStatus = HttpStatus.OK;
                response = new Response<>(
                        HTTPMessage.getSuccessMessage(Table.USERS, Transaction.SUCCESS_DELETED),
                        true
                );
            }
            else {
                httpStatus = HttpStatus.BAD_REQUEST;
                response = new ResponseFailure<>(
                        HTTPMessage.getFailureMessage(Table.USERS, Transaction.FAILURE_DELETE),
                        false,
                        ResponseHTTPStatus.FAIL_DELETED
                );
            }
        }
        catch (Exception e) {
            e.printStackTrace();
            httpStatus = HttpStatus.INTERNAL_SERVER_ERROR;
            response = new ResponseRecordFailure<>(
                    HTTPMessage.getFailureMessage(Table.USERS, Transaction.FAILURE_DELETE),
                    false,
                    ResponseHTTPStatus.INTERNAL_SERVER_ERROR
            );
        }

        return new ResponseEntity<>(response, httpStatus);
    }

    @PostMapping
    private ResponseEntity<Response<User>> addUser(@RequestBody UserForm user) {
        Response<User> response;

        try {
            User addedUser = userService.addUser(user);
            if (addedUser != null) {
                httpStatus = HttpStatus.OK;
                response = new ResponseRecord<>(
                        HTTPMessage.getSuccessMessage(Table.USERS, Transaction.SUCCESS_CREATED),
                        true,
                        addedUser
                );
            }
            else {
                httpStatus = HttpStatus.BAD_REQUEST;
                response = new ResponseRecordFailure<>(
                        HTTPMessage.getFailureMessage(Table.USERS, Transaction.FAILURE_CREATE),
                        false,
                        ResponseHTTPStatus.FAIL_CREATED
                );
            }
        }
        catch (Exception e) {
            e.printStackTrace();
            httpStatus = HttpStatus.INTERNAL_SERVER_ERROR;
            response = new ResponseRecordFailure<>(
                    HTTPMessage.getFailureMessage(Table.USERS, Transaction.FAILURE_CREATE),
                    false,
                    ResponseHTTPStatus.INTERNAL_SERVER_ERROR
            );
        }

        return new ResponseEntity<>(response, httpStatus);
    }
}
