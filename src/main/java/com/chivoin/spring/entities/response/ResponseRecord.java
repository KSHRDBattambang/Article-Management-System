package com.chivoin.spring.entities.response;

/**
 * Created by Obi-Voin Kenobi on 05-Jul-17.
 */
public class ResponseRecord<T> extends Response {

    private T data;

    public ResponseRecord() { }

    public ResponseRecord(String message, boolean status, T data) {
        super(message, status);
        this.data = data;
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }
}
